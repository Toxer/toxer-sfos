/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <QGuiApplication>
#include <QQmlEngine>
#include <QQmlContext>
#include <QQuickView>
#include <QScreen>
#include <QThread>
#include <QTranslator>

#include "ToxerSailfish.h"

using Toxer::Api;

int main(int argc, char *argv[]) {
  setenv("QT_QUICK_CONTROLS_STYLE", "Nemo", 1);
  QQuickWindow::setDefaultAlphaBuffer(true);

  QGuiApplication app(argc, argv);
  app.thread()->setObjectName(QStringLiteral("MainThread"));
  app.setOrganizationName(QStringLiteral("Tox"));
  app.setApplicationName(QStringLiteral("Toxer"));
  app.setApplicationVersion(QStringLiteral(TOXER_VERSION));

  QTranslator translator;
  { // setup translator
    auto qm_dir = Toxer::pathTo(QStringLiteral("translation")).toLocalFile();
    auto qm_prefix = QStringLiteral("toxer_sfos");
    auto qm_separator = QStringLiteral("_");
    if (translator.load(QLocale(), qm_prefix, qm_separator, qm_dir)) {
      app.installTranslator(&translator);
    } else {
      qDebug("Looking for translations in: %s", qUtf8Printable(qm_dir));
      qInfo("No translation for locale %s.", qUtf8Printable(QLocale().name()));
    }
  }

  Toxer::registerQmlTypes();

  Api toxer;

#ifdef SAILFISHAPP
  std::unique_ptr<QQuickView> sf_view(SailfishApp::createView());
  QQuickView& view = *sf_view;
#else
  QQuickView view;
#endif

  QObject::connect(&toxer, &Api::profileChanged, [&toxer, &view]() {
    auto q = toxer.hasProfile() ? QStringLiteral("qml/MainView.qml")
                                : QStringLiteral("qml/ToxerMain.qml");
    auto url = Toxer::pathTo(q);

    // important: let the event loop do the layout correctly
    QMetaObject::invokeMethod(&view, "setSource", Qt::QueuedConnection, Q_ARG(QUrl, url));
  });

  auto engine = view.engine();
  QObject::connect(engine, &QQmlEngine::quit, &app, &QGuiApplication::quit);
  engine->addImportPath(Toxer::pathTo(QStringLiteral("qml")).toString());

  view.setResizeMode(QQuickView::SizeRootObjectToView);
  view.setTitle(app.applicationDisplayName());
  view.rootContext()->setContextProperty(QStringLiteral("Toxer"), &toxer);
  view.setSource(Toxer::pathTo(QStringLiteral("qml/ToxerMain.qml")));
  view.show();
  return app.exec();
}
