#!/bin/bash
set -e

# Sailfish host environment
SELF=$(dirname `readlink -f $0`)
source $SELF/mersdk/env
SSH_KEYDIR="~/SailfishOS/vmshare/ssh/private_keys/engine/mersdk"
SSH_SF="ssh -q -p 2222 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $SSH_KEYDIR"
extradir=$SELF/extra # lib binaries go here

function trapped() {
  echo ""
  echo ""
  echo "Script aborted. Bye!"
  untrap # avoid recursion
  exit 1
}
function set_trap() {
  trap trapped INT TERM EXIT
}
function untrap() {
  trap - INT TERM EXIT
}

function sf_sdk() {
# Runs a command in Sailfish SDK VM.
# $1: the shell command
  $SSH_SF mersdk@localhost "$1"
}

function sf_sdk_rsync() {
# Copies files from/to Sailfish SDK VM.
  rsync -a -e "$SSH_SF" --delete-before --delete-excluded $@
}

set_trap

echo -en "Installing Sailfish SDK build dependencies… \t"
sf_sdk "sb2 -t SailfishOS-${SFVER}-i486    -m sdk-install -R zypper -n install gcc gcc-c++ autoconf automake make cmake libtool git" >/dev/null
sf_sdk "sb2 -t SailfishOS-${SFVER}-armv7hl -m sdk-install -R zypper -n install gcc gcc-c++ autoconf automake make cmake libtool git" >/dev/null
echo "OK"

echo -en "Preparing Toxer build environment in Sailfish SDK… \t"
sf_sdk 'mkdir -p ~/tox'
sf_sdk_rsync $SELF/mersdk/build-libs.sh $SELF/mersdk/env mersdk@localhost:~/tox >/dev/null
echo "OK"

# Build Tox libraries
sf_sdk "~/tox/build-libs.sh i486 && ~/tox/build-libs.sh armv7hl"

echo -en "Copying build artifacts to $extradir \t\t"
mkdir -p $extradir
sf_sdk_rsync \
  --exclude "bin/" --exclude "pkgconfig/" --exclude "*.la" --exclude "*.a" \
  mersdk@localhost:~/tox/*i486/* \
  $extradir/i486 \
  >/dev/null
sf_sdk_rsync \
  -L --no-links \
  --exclude "bin/" --exclude "pkgconfig/" --exclude "*.so.*.*" --exclude "*.la" --exclude "*.a" \
  mersdk@localhost:~/tox/*armv7hl/* \
  $extradir/armv7hl \
  >/dev/null
echo "OK"

echo ""
echo "Toxer developer environment for Sailfish $SFVER ready!"
echo "Run the following command in MerSDK to build the package:"
echo "  1. Emulator: mb2 -t SailfishOS-${SFVER}-i486 build"
echo "  2. Device  : mb2 -t SailfishOS-${SFVER-armv7hl} build"
echo ""
echo "The resulting package will be in ${SELF}/RPMS."
echo ""
echo "Need further help with Sailfish SDK?"
echo "Visit: https://sailfishos.org/wiki/"
echo ""
echo "Last not least: Have fun with Toxer! Bye!"

untrap
