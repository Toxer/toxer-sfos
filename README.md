# About Toxer for Sailfish
This is the Sailfish user interface implementation based on Toxer - a multi-platform Tox client framework with focus on QtQuick applications.

*Please note the app and framework is still highly being developed and may have major flaws. Neither can it be considered feature complete. If you feel like contributing to a fun and high quality messenger project, don't hesitate and open a merge request on Gitlab.*

How does it look like you ask? Some impressions taken on a real phone:

Safe | Simple to use | Customizable.
---- | ---- | ----
<img src="webres/toxer.png" alt="Toxer start page" width="240px" /> | <img src="webres/friendlist.png" alt="Toxer friend list" width="240px" /> | <img src="webres/appearance.png" alt="Toxer appearance page" width="240px" />

Do you prefer native Sailfish look and feel? Go to Toxer settings (via "pulley" menu on the top). Then pull the opacity slider to the left. Have fun sailing. 😊

# HowTo build Toxer for Sailfish

The following prerequisites are required, before you can build Toxer-SFOS:

* Make sure SailfishSDK is installed and accessible via SSH.
* Clone the [Toxer main repository](https://gitlab.com/Toxer/toxer-dev-env) (**not this one!**) to a directory of choice:

```bash
cd /my/projects
git clone https://gitlab.com/toxer/toxer-dev-env.git Toxer
cd Toxer
./setup-dev.sh init

# The script creates a self-contained project workspace by performing the following steps:
# 1. Required repositories are cloned in the "Toxer" project directory.
# 2. Symlinks to "toxer_core" are created in the subprojects to keep dependencies lean.
```

## Step 1: Setup the SailfishSDK & Projekt Directory

Building Toxer for Sailfish is straightforward:

```bash
# Example (replace with the actual Sailfish target version)
SFVER='3.1.0.12' ./setup-sfsdk.sh

# This will install required development packages (CMake, Git, etc.) in SailfishSDK.
# Further it will install toxcore + sodium libraries in SailfishSDK in "~/tox" directory.
# The resulting binaries are finally copied to the "extra" subfolder.
```

Here's an overview of the additional packages that are installed inside SailfishSDK:

Package(s) | Notes
---- | ----
gcc, gcc-c++ | C/C++ compiler
make | good old Make
cmake | CMake build tooling
autoconf, automake | required for libsodium
libtool | required for libsodium
git-minimal | used to add the Git hash to the Toxer version

**Note:** After upgrading SailfishSDK to the latest version run `setup-sfsdk.sh` again.

## Step 2: Build the RPM Package

To build the RPM package manually SSH to SailfishSDK and call the `mb2` build command:

```bash
# ssh to the SailfishSDK…
ssh -p 2222 -i ~/SailfishOS/vmshare/ssh/private_keys/engine/mersdk mersdk@localhost

# …and build the RPM package.
cd /home/src1/toxer-sfos  # cd to /your/toxer-sfos project directory
mb2 -t SailfishOS-<sfos_version>-<target> build
```

### (Optional) Add an SSH Configuration

Configuring your local SSH setup in "~/.ssh/config" makes it less of a hastle to connect to the SailfishSDK/-Emulator.

```
# SailfishSDK
Host sfsdk
HostName localhost
User mersdk
IdentityFile ~/SailfishOS/vmshare/ssh/private_keys/engine/mersdk
Port 2222
StrictHostKeyChecking no
UserKnownHostsFile /dev/null

# Sailfish emulator (latest version)
Host sfemu
HostName localhost
Port 2223
User root
IdentityFile ~/SailfishOS/vmshare/ssh/private_keys/Sailfish_OS-Emulator-latest/root
StrictHostKeyChecking no
UserKnownHostsFile /dev/null
```

You can now connect by writing `ssh sfsdk` and `ssh sfemu`.
