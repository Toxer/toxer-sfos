<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AddFriendView</name>
    <message>
        <source>Enter your friend&apos;s Tox-ID…</source>
        <translation type="vanished">Tox-ID des Freundes eingeben…</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Schließen</translation>
    </message>
    <message>
        <source>Enter a Tox address.</source>
        <translation type="vanished">Tox-Adresse eingeben.</translation>
    </message>
    <message>
        <source>Hi,

please add me as Tox friend.

My Tox-Name: %1
My Tox-Address: %2

Thanks, cheers!

Don&apos;t know Tox? Get information at %3.</source>
        <translation></translation>
    </message>
    <message>
        <source>Send Tox Invitation</source>
        <translation type="vanished">Tox-Einladung Senden</translation>
    </message>
    <message>
        <source>enter Tox-Address</source>
        <translation>Tox-Adresse eingeben</translation>
    </message>
    <message>
        <source>Send Tox-Invitation</source>
        <translation>Tox-Einladung Senden</translation>
    </message>
</context>
<context>
    <name>Create</name>
    <message>
        <source>Enter a Tox name…</source>
        <translation type="vanished">Tox-Namen eingeben…</translation>
    </message>
    <message>
        <source>Location: %1/%2</source>
        <translation>Speicherort: %1/%2</translation>
    </message>
    <message>
        <source>Enter a profile&apos;s password…</source>
        <translation type="vanished">Ein Profilpasswort festlegen…</translation>
    </message>
    <message>
        <source>Retype the profile&apos;s password…</source>
        <translation type="vanished">Profilpasswort wiederholen…</translation>
    </message>
    <message>
        <source>Create Profile</source>
        <translation>Profil Erstellen</translation>
    </message>
    <message>
        <source>Enter a Tox alias.</source>
        <translation type="vanished">Tox-Alias eingeben.</translation>
    </message>
    <message>
        <source>Enter a profile password…</source>
        <translation type="vanished">Profilpasswort eingeben.</translation>
    </message>
    <message>
        <source>Retype the profile&apos;s password.</source>
        <translation type="vanished">Profilpasswort wiederholen</translation>
    </message>
    <message>
        <source>enter Tox-Alias</source>
        <translation>Tox-Alias eingeben</translation>
    </message>
    <message>
        <source>enter profile password</source>
        <translation>Profilpasswort eingeben</translation>
    </message>
    <message>
        <source>retype profile password</source>
        <translation>Profilpasswort wiederholen</translation>
    </message>
</context>
<context>
    <name>Import</name>
    <message>
        <source>&lt;h2&gt;This part of Toxer is work in progress!&lt;/h2&gt;&lt;p&gt;Want to help out?&lt;/p&gt;&lt;p&gt;Visit &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Invite Friend</source>
        <translation>Tox-Freund Einladen</translation>
    </message>
    <message>
        <source>Close Tox Profile</source>
        <translation type="vanished">Profil Schließen</translation>
    </message>
    <message>
        <source>Close Profile</source>
        <translation>Profil Schließen</translation>
    </message>
</context>
<context>
    <name>MainViewSlim</name>
    <message>
        <source>Settings</source>
        <translation type="vanished">Einstellungen</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Schließen</translation>
    </message>
</context>
<context>
    <name>MessengerView</name>
    <message>
        <source>Type your message here...</source>
        <translation>Nachricht eingeben…</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Senden</translation>
    </message>
</context>
<context>
    <name>ProfilesPage</name>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Start Profile</source>
        <translation>Profil Starten</translation>
    </message>
    <message>
        <source>New Profile</source>
        <translation>Neues Profil</translation>
    </message>
    <message>
        <source>Import Profile</source>
        <translation>Profil Importieren</translation>
    </message>
</context>
<context>
    <name>Select</name>
    <message>
        <source>Profile:</source>
        <translation>Profil:</translation>
    </message>
    <message>
        <source>Enter the profile password…</source>
        <translation>Profilpasswort eingeben…</translation>
    </message>
    <message>
        <source>Open profile %1</source>
        <translation>Profil %1 öffnen</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Toxer Settings</source>
        <translation>Toxer Einstellungen</translation>
    </message>
    <message>
        <source>Use Dark Icons</source>
        <translation>Dunkle Icons Verwenden</translation>
    </message>
    <message>
        <source>Switches the dark/light icon &amp; text theming.</source>
        <translation>Schaltet dunkles/helles Icon &amp; Text Thema um.</translation>
    </message>
    <message>
        <source>Opacity</source>
        <translation>Deckkraft</translation>
    </message>
    <message>
        <source>Red</source>
        <translation>Rot</translation>
    </message>
    <message>
        <source>Green</source>
        <translation>Grün</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation>Blau</translation>
    </message>
</context>
</TS>
