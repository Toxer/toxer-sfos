<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AddFriendView</name>
    <message>
        <source>Hi,

please add me as Tox friend.

My Tox-Name: %1
My Tox-Address: %2

Thanks, cheers!

Don&apos;t know Tox? Get information at %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>enter Tox-Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send Tox-Invitation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Create</name>
    <message>
        <source>Location: %1/%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>enter Tox-Alias</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>enter profile password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>retype profile password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Import</name>
    <message>
        <source>&lt;h2&gt;This part of Toxer is work in progress!&lt;/h2&gt;&lt;p&gt;Want to help out?&lt;/p&gt;&lt;p&gt;Visit &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invite Friend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close Profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessengerView</name>
    <message>
        <source>Type your message here...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfilesPage</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import Profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Select</name>
    <message>
        <source>Profile:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter the profile password…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open profile %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Toxer Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use Dark Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switches the dark/light icon &amp; text theming.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Blue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
