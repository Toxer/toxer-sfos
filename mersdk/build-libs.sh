#!/bin/bash

set -e

# Sailfish SDK environment
SELF=$(dirname `readlink -f $0`)
source "$SELF/env"

if [ "$#" -ne 1 ]; then
  echo -e "\033[1;31mError: Invalid number of parameters.\033[m"
  exit 1
fi
if [ "$1" != "i486" ] && [ "$1" != "armv7hl" ]; then
  echo -e "\033[1;31mError: Sailfish platform must be one of armv7hl or i486. Neither was provided!\033[m"
  exit 2
fi
SFPLATFORM=$1

# note: vars are valid in Sailfish SDK (virtual machine)
TARGET=$SFVER-$SFPLATFORM
FAKEDIR=$SELF/$TARGET
SODIUMDIR=$SELF/libsodium
SODIUMLIBSDIR="$SODIUMDIR/src/libsodium/.libs"
TOXCOREDIR=$SELF/c-toxcore
TOXCORELIBSDIR=$TOXCOREDIR/build/.libs

echo "Building libraries for target ${TARGET}."
echo ""
echo ""

echo -en "Creating fake root $TARGET \t\t"
rm -rf $FAKEDIR && mkdir $FAKEDIR
rm -rf "$SODIUMDIR" && mkdir -p "$SODIUMDIR"
rm -rf "$TOXCOREDIR" && mkdir -p "$TOXCOREDIR"
echo "OK"

echo "Getting sources:"
echo -en " -> libsodium $SODIUMVER \t\t\t\t"
curl -s -L "$SODIUMSRC" | tar xz -C "$SODIUMDIR" --strip-components=1 > /dev/null
echo "OK"

echo -en " -> toxcore $TOXCOREVER \t\t\t\t"
curl -s -L "$TOXCORESRC" | tar xz -C "$TOXCOREDIR" --strip-components=1 > /dev/null
echo "OK"

echo -en "Building sodium \t\t\t\t"
cd $SODIUMDIR
sb2 -t SailfishOS-$TARGET -m sdk-build autoreconf -i                 > $FAKEDIR/sodium.out 2> $FAKEDIR/sodium.err
sb2 -t SailfishOS-$TARGET -m sdk-build ./configure --prefix $FAKEDIR >>$FAKEDIR/sodium.out 2>>$FAKEDIR/sodium.err
sb2 -t SailfishOS-$TARGET -m sdk-build make -s clean                 >>$FAKEDIR/sodium.out 2>>$FAKEDIR/sodium.err
sb2 -t SailfishOS-$TARGET -m sdk-build make -s                       >>$FAKEDIR/sodium.out 2>>$FAKEDIR/sodium.err
echo "OK"
echo -en "Installing to $FAKEDIR \t"
sb2 -t SailfishOS-$TARGET -m sdk-build make install >$SELF/sodium.install.out
echo "OK"

echo -en "Building toxcore \t\t\t\t"
cd $TOXCOREDIR
sb2 -t SailfishOS-$TARGET -m sdk-build cmake \
 -DCMAKE_BUILD_TYPE=Release \
 -DCMAKE_C_FLAGS_RELEASE='-O3' \
 -DCMAKE_CXX_FLAGS_RELEASE='-O3' \
 -DCMAKE_INSTALL_PREFIX=$FAKEDIR \
 -DCMAKE_PREFIX_PATH=$FAKEDIR \
 -DBUILD_TOXAV=OFF \
 -DBUILD_AV_TEST=OFF \
 -DPKG_CONFIG_USE_CMAKE_PREFIX_PATH=1 \
 -- . > $FAKEDIR/tox.out 2> $FAKEDIR/tox.err
sb2 -t SailfishOS-$TARGET -m sdk-build cmake --build . >>$FAKEDIR/tox.out 2>>$FAKEDIR/tox.err
echo "OK"
echo -en "Installing to $FAKEDIR \t"
sb2 -t SailfishOS-$TARGET -m sdk-build make install >$SELF/toxcore.install.out
echo "OK"
