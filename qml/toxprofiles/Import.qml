import QtQuick 2.5
import QtGraphicalEffects 1.0

import animations 1.0
import controls 1.0 as Controls
import style 1.0

Item {
    id: root

    Rectangle {
        id: brect
        anchors.fill: parent
        width: 300
        height: txt.implicitHeight + (txt.padding * 2)
        color: "#bb77bbbb"
        radius: 9
        border.color: "#a0cc3333"
        border.width: 2
    }
    RadialGradient {
        id: bradient
        anchors.fill: brect
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#ddd" }
            GradientStop { position: 1.0; color: "transparent" }
        }
    }
    Text {
        id: txt
        width: root.width
        anchors.centerIn: parent
        anchors.margins: 10
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.WordWrap
        textFormat: TextEdit.RichText
        color: "#333"
        text: {
            var link = "https://gitlab.com/Toxer/toxer-sfos";
            return qsTr("<h2>This part of Toxer is work in progress!</h2>" +
                        "<p>Want to help out?</p>" +
                        "<p>Visit <a href='%1'>%1</a>" +
                        " for more information.</p>").arg(link)
        }
        onLinkActivated: { Qt.openUrlExternally(link) }
    }

    // animations
    Pulsing {
        target: bradient
        properties: "horizontalRadius, verticalRadius"
        from: brect.height / 1.5
        to: brect.height
        durationIn: 500
        loops: Animation.Infinite
        running: true
    }
    Ringing {
        target: brect
        duration: 1800
        loops: Animation.Infinite
        running: true
    }
}
