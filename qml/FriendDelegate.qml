/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.2
import QtGraphicalEffects 1.0

import animations 1.0 as Animations
import controls 1.0 as Controls
import style 1.0

Item {
    id: root

    implicitWidth: delegateRow.width
    implicitHeight: delegateRow.height

    property alias name: name
    property alias avatar: avatar
    property alias statusMessage: statusMessage
    property alias statusLight: statusLight
    property int unreadMessages: 0

    Animations.Pulsing {
        target: messageIndicator
        properties: "spread"
        from: 0.3
        to: 0.75
        loops: Animation.Infinite
        running: messageIndicator.visible
    }

    Row {
        id: delegateRow
        spacing: 0

        Item {
            id: avatarItem
            height: parent.height
            width: height + statusLight.width

            Image {
                id: avatar

                visible: false
                height: parent.height
                width: height
                fillMode: Image.PreserveAspectFit
                sourceSize.width: width
                sourceSize.height: height
            }
            Rectangle {
                id: avatarMask

                anchors.fill: avatar
                color: Style.color.alternateBase
                radius: width / 2
                OpacityMask {
                    anchors.fill: parent
                    source: avatar
                    maskSource: avatarMask
                }
            }

            Image {
                id: statusLight

                visible: true
                height: Math.max(avatar.height * 0.33, 10)
                width: height
                anchors.bottom: avatar.bottom
                anchors.right: avatar.right
                anchors.rightMargin: -(height / 2)
                fillMode: Image.PreserveAspectFit
                sourceSize.width: width
                sourceSize.height: height
            }
            Glow {
                id: messageIndicator

                visible: unreadMessages > 0
                anchors.fill: statusLight
                radius: 10
                samples: 17
                color: "#6a3"
                source: statusLight
            }
        }

        Column {
            Controls.Text {
                id: name
                font.bold: true
                text: "<alias>"
            }
            Controls.Text {
                id: statusMessage
                text: "<status_message>"
            }
        }
    }
}
