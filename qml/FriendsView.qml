/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.2
import QtGraphicalEffects 1.0
import Sailfish.Silica 1.0 as SF

import Toxer 1.0
import Toxer.Types 1.0

import base 1.0 as Base
import style 1.0

Base.View {
    id : root

    ListView {
        id: friendsView

        property bool compactMode: false

        ToxFriends {
            id: tfq

            function statusIcon(friendIndex) {
                if (isOnline(friendIndex)) {
                    switch (statusInt(friendIndex)) {
                    case ToxTypes.Unknown:
                    case ToxTypes.Away: return Style.icon.away;
                    case ToxTypes.Busy: return Style.icon.busy;
                    case ToxTypes.Ready: return Style.icon.online;
                    }

                    return Style.icon.away;
                } else {
                    return Style.icon.offline;
                }
            }
        }

        anchors.fill: parent
        spacing: compactMode ? 1 : 2
        model: tfq.list.slice(0)
        delegate: SF.Drawer {
            id: friendDelegate
            width: friendsView.width
            height: friendItem.height
            dock: SF.Dock.Right
            open: index === friendsView.currentIndex
            backgroundSize: friendMenuRow.width
            background: Item {
                anchors.fill: parent
                Rectangle {
                    id: friendMenuBackground
                    visible: false
                    anchors.fill: parent
                    color: Style.color.alternateBase
                }
                InnerShadow {
                    fast: true
                    source: friendMenuBackground
                    anchors.fill: friendMenuBackground
                    radius: 14.0
                    samples: 16
                    horizontalOffset: 8
                    verticalOffset: 8
                    color: "#000000"
                }
                Row {
                    anchors.verticalCenter: parent.verticalCenter
                    id: friendMenuRow
                    spacing: SF.Theme.paddingMedium
                    SF.IconButton {
                        icon { source: Style.icon.chat; sourceSize: Qt.size(parent.height/3, parent.height/3) }
                        onClicked: {
                            if (viewLoader) {
                                viewLoader.setSource("MessengerView.qml", {
                                                         friendIndex: index
                                                     });
                            }
                        }
                    }
                }
            }

            FriendDelegate {
                id: friendItem
                name.text: tfq.name(modelData)
                statusMessage.visible: !friendsView.compactMode
                statusMessage.text: tfq.statusMessage(modelData)
                avatar.source: {
                    var pk = tfq.publicKeyStr(modelData);
                    var url = Toxer.avatarsUrl() + "/" + pk.toUpperCase() + ".png";
                    return Toxer.exists(url) ? url : Style.icon.noAvatar;
                }
                statusLight.source: tfq.statusIcon(modelData);

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        friendsView.currentIndex = index;
                    }
                }

                Connections {
                    target: tfq
                    onIsOnlineChanged: {
                        if (index === modelData) {
                            // TODO: get icon by connection state
                            statusLight.source = tfq.statusIcon(index);
                        }
                    }
                    onMessage: {
                        if (index === modelData) {
                            unreadMessages++;
                        }
                    }
                    onStatusChanged: {
                        if (index === modelData) {
                            // TODO: get icon by state enum
                            statusLight.source = tfq.statusIcon(index);
                        }
                    }
                    onNameChanged: {
                        if (index === modelData) {
                            friendItem.name.text = name;
                        }
                    }
                    onStatusMessageChanged: {
                        if (index === modelData) {
                            friendItem.statusMessage.text = statusMessage;
                        }
                    }
                }
            }
        }

        clip: true
        focus: true
        highlightFollowsCurrentItem: false
        highlight: Rectangle {
            y: friendsView.currentItem.y
            z: friendsView.currentItem.z + 1
            width: friendsView.currentItem.width
            height: friendsView.currentItem.height
            border.color: "#80FFFFFF"
            color: "#33000000"

            Behavior on y {
                NumberAnimation { duration: 120 }
            }
        }
    }
}
