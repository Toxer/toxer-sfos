/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.0

import Toxer 1.0

import base 1.0 as Base
import controls 1.0 as Controls
import style 1.0

Base.View {
  id: root
  width: 320
  height: 600
  Component.onCompleted: messageInput.forceActiveFocus();

  property int friendIndex: my_friend.fno

  function sendMessage() {
    var message = messageInput.text;
    if (message) {
      my_friend.sendMessage(friendIndex, message);
      messageModel.addMessageItem(tpq.publicKeyStr(), message);
      messageInput.clear();
    }
  }

  ToxProfileQuery { id: tpq }
  ToxFriend {
    id: my_friend
    onMessage: messageModel.addMessageItem(my_friend.publicKeyStr(index), message);
  }
  ListModel {
    id: messageModel
    Component.onCompleted: {
      addMessageItem("", "Note from Toxer:\nToxer messaging is work in progress! The messages you send or receive from friends will not be saved in any way and be lost when leaving the chat view (e.g. by chosing another friend).\n\nThe Toxer developers!");
    }
    function addMessageItem(pk, message) {
      var ts = new Date().getTime();
      append({ pk: pk, ts: ts, message: message });
    }
  }

  Column {
    id: column
    anchors.fill: parent

    ListView {
      id: messageBox
      width: column.width
      height: column.height - inputLayout.height
      spacing: 2
      clip: true
      model: messageModel
      delegate: MessengerDelegate {
        width: messageBox.width
        layoutDirection: tpq.isMe(pk) ? Qt.RightToLeft : Qt.LeftToRight
        bubbleColor: tpq.isMe(pk) ? "#5E97EB" : "#424954"
        avatarSource: {
          var url = Toxer.avatarsUrl() + "/" + pk.toUpperCase() + ".png";
          return Toxer.exists(url) ? url : Style.icon.noAvatar;
        }
      }

      // auto-scroll to last item
      currentIndex: messageModel.count - 1
    }
    Row {
      id: inputLayout
      spacing: 0
      clip: true

      Controls.TextArea {
        id: messageInput
        width: column.width - btnSendMessage.width
        placeholderText: qsTr("Type your message here...")
        wrapMode: TextEdit.WordWrap
      }

      Controls.Button {
        id: btnSendMessage
        enabled: messageInput.text
        text: qsTr("Send")
        onClicked: root.sendMessage();
      }
    }
  }
}
