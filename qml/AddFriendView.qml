/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2018 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.0

import Toxer 1.0

import base 1.0 as Base
import controls 1.0 as Controls

Base.View {
    id: root

    ToxProfileQuery { id: tpq }

    Column {
        id: column
        anchors.fill: parent

        Controls.TextField {
            id: txtToxID
            width: column.width
            placeholderText: qsTr("enter Tox-Address")
        }

        Controls.TextArea {
            id: txtMessage
            width: column.width
            wrapMode: TextEdit.Wrap
            text: placeholderText
            placeholderText: {
                qsTr("Hi,\n\nplease add me as Tox friend.\n\nMy Tox-Name: %1\nMy Tox-Address: %2\n\nThanks, cheers!\n\nDon't know Tox? Get information at %3.")
                .arg(tpq.userName())
                .arg(tpq.addressStr().toUpperCase())
                .arg("https://tox.chat");
            }
        }

        Controls.Button {
            id: btnClose
            anchors.horizontalCenter: column.horizontalCenter
            Accessible.defaultButton: true
            text: qsTr("Send Tox-Invitation");
            enabled: txtToxID.text // TODO: also check that Tox-ID is valid
            onClicked: {
                var msg = txtMessage.text ? txtMessage.text : txtMessage.placeholderText;
                tpq.inviteFriend(txtToxID.text, msg);

                // close the view after invitation has been sent
                closing();
            }
        }
    }
}
