/*
Copyright (C) 2013 Jolla Ltd.
Contact: Thomas Perl <thomas.perl@jollamobile.com>
All rights reserved.

You may use this file under the terms of BSD license as follows:

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of the Jolla Ltd nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.5
import Sailfish.Silica 1.0 as SF

import base 1.0 as Base
import controls 1.0 as Controls
import style 1.0

Base.Page {
    id: page

    SF.SilicaFlickable {
        id: listView
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column
            width: parent.width

            SF.PageHeader {
                title: qsTr("Toxer Settings")
            }

            SF.TextSwitch {
                width: column.width
                text: qsTr("Use Dark Icons")
                description: qsTr("Switches the dark/light icon & text theming.")
                checked: Style.lightTheme
                onCheckedChanged: {
                    Style.lightTheme = checked;
                }
            }

            Controls.Slider {
                width: column.width

                label: qsTr("Opacity");
                value: Style.color.base.a

                onValueChanged: {
                    Style.color.base.a = value;
                }
            }
            Controls.Slider {
                width: column.width

                label: qsTr("Red")
                from: 0.1
                to: 0.9
                value: Style.color.base.r

                onValueChanged: {
                    Style.color.base.r = value
                }
            }
            Controls.Slider {
                width: column.width

                label: qsTr("Green")
                from: 0.1
                to: 0.9
                value: Style.color.base.g

                onValueChanged: {
                    Style.color.base.g = value
                }
            }
            Controls.Slider {
                width: column.width

                label: qsTr("Blue")
                from: 0.1
                to: 0.9
                value: Style.color.base.b

                onValueChanged: {
                    Style.color.base.b = value
                }
            }
        }
    }
}
